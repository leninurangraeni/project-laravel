@extends('layout.master')
@section('judul')
    Halaman Welcome
@endsection

@section('isi')
    <h1>Selamat Datang di Sanbercode {{$nama}}</h1>
    <h2>Terima kasih telah bergabung di SanberBook. Sosial Media kita bersama!</h2>
@endsection