@extends('layout.master')
@section('judul')
    Halaman Register
@endsection

@section('isi')
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label> <br><br>
        <input type="text" name "name"> <br><br>
        <label>Last Name</label> <br><br>
        <input type="text" name "name"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="Male" >Male <br><br>
        <input type="radio" name="Female">Female <br><br>
        <input type="radio" name="Other" >Other <br><br>
        <label>Nationality</label> <br><br>
        <select name="Nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singapure</option>
            <option value="3">Inggris</option> 
        </select>  <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br><br>
        <input type="checkbox"> English <br><br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">

    </form>
    

@endsection